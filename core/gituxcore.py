import os
import sys
import time
import urllib.request
import subprocess
from subprocess import check_output as inputstream

current_dir = os.getcwd()
# Banner
gitux_banner = """
   banner here
"""
backtomenu_banner = """
  [99] Back To Main Menu
  [00] Exit Gitux
"""

prefix = os.getenv("PREFIX")
configBase = "[HOME] = ~"
configFile = "../gituc.conf"
cache_1 = prefix + "/tmp/gitux_1"


def repo_check(sources_list):
    if os.path.isfile(os.getenv("PREFIX") + "/etc/apt/sources.list.d/" + sources_list):
        return True
    return False


def writeStatus(statusId):
    open(cache_1, "w").write(str(statusId))


def readStatus():
    try:
        statusId = open(cache_1, "r").read()
        if statusId == "1":
            return True
        return False
    except IOError:
        return False


def checkConfigFile():
    if os.path.exists(configFile):
        if os.path.isdir(configFile):
            os.system(f"rm -rf {configFile}")
            open(configFile, "w").write(configBase)
    else:
        open(configFile, "w").write(configBase)


def loadConfigFile():
    checkConfigFile()
    # noinspection PyUnusedLocal
    lfile = ""
    # noinspection PyUnusedLocal
    try:
        lfile = [x.split("=")[-1].strip() for x in open(configFile, "r").splitlines() if
                 x.split("=")[0].strip() == "[HOME]"][0]
    except Exception as e:
        lfile = "~"
    return lfile


homeDir = loadConfigFile()


def restart_program():
    python = sys.executable
    os.execl(python, python, *sys.argv)
    # noinspection PyUnusedLocal
    curdir = os.getcwd()

 # Back to main
def backtomenu_option():
    if not readStatus():
        print(backtomenu_banner)
        backtomenu = input("Gitux > ")

        if backtomenu == "99":
            restart_program()
        elif backtomenu == "00":
            sys.exit()
        else:
            print("\nERROR: Wrong Input!")
            time.sleep(2)
            restart_program()


def banner():
    print(gitux_banner)


def pointless_repo():
    urllib.request.urlretrieve('https://its-pointless.github.io/setup-pointless-repo.sh',
                               'setup-pointless-repo.sh')
    os.system('bash setup-pointless-repo.sh')
    os.remove('setup-pointless-repo.sh')
    os.system('apt update -y && apt upgrade -y')


###
def gtxinstgit():
  print('##### Installing Git')
  os.system('pkg update -y && pkg upgrade -y')
  os.system('pkg install git')
  print('##### Git Inatall Complete')
  time.sleep(2)
  backtomenu_option()
  
def gtxconfgitusr():
  print('\n#####Setting Up Username And Email')
  time.sleep(1)
  gitusrname = raw_input('Enter Username: ')
  os.system("git config --global user.name '{0}'".format(gitusrname))
  gitusremail = raw_input('Enter E-mail: ')
  os.system("git config --global user.email '{0}'".format(gitusremail))
  os.system('clear')
  os.system('git config')
  time.sleep(3)
  backtomenu_option()
  
def nmap():
    print('\n###### Installing Nmap')
    os.system('apt update -y && apt upgrade -y')
    os.system('apt install nmap')
    print('###### Done')
    print("###### Type 'nmap' to start.")
    backtomenu_option()


def red_hawk():
    print('\n###### Installing RED HAWK')
    os.system('apt update -y && apt upgrade -y')
    os.system('apt install git php')
    os.system('git clone https://github.com/Tuhinshubhra/RED_HAWK')
    os.system('mv RED_HAWK {}'.format(homeDir))
    print('###### Done')
    backtomenu_option()


def dtect():
    print('\n###### Installing D-TECT')
    os.system('apt update -y && apt upgrade -y')
    os.system('apt install python2 git')
    os.system('git clone https://github.com/bibortone/D-Tech')
    os.system('mv D-Tech {}/D-TECT'.format(homeDir))
    print('###### Done')
    backtomenu_option()


def sqlmap():
    print('\n###### Installing sqlmap')
    os.system('apt update -y && apt upgrade -y')
    os.system('apt install git python2')
    os.system('git clone https://github.com/sqlmapproject/sqlmap')
    os.system('mv sqlmap {}'.format(homeDir))
    print('###### Done')
    backtomenu_option()



